import statsmodels.formula.api as smf
import pandas as pd
import numpy as np
import sqlite3
import gc
import matplotlib.pyplot as plt
import pickle
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA

database = 'test/price_optimization.db'
conn = sqlite3.connect(database)

def pca(df):
    """PCA with scikit learn."""
    dim = df.shape[0]-20
    X_map = StandardScaler().fit_transform(df)
    pca_comps = PCA(n_components=dim)
    pca_comps.fit_transform(X_map)

def features_setup(df_store_data,headers=['week','units','dollars','upc','pr'],
        output_features=['dollars','pr']):
    """Arrange data in columns with variable = upc + variable type."""
    
    # get all products, i.e., all unique upcs
    upcs = df_store_data['upc'].unique().tolist()
    
    # Group by week, then create rows in week where upc is missing 
    groups = df_store_data.groupby('week')
    upc_index = pd.Index(upcs,name='upc')
    df_modified_groups = []
    for i, group in groups:
        # get the group week
        week = group['week'].unique()[0]

        # insert rows for missing upcs and restore the group week
        mod_group = group.set_index(['upc']).reindex(upc_index).reset_index()
        mod_group['week'] = week

        df_modified_groups.append(mod_group.copy())

    # recombine groups and sort by week 
    df_modified_groups = pd.concat(df_modified_groups).sort_values('week')

    # pivot the data to put features in columns
    df_modified_groups = df_modified_groups[headers].pivot('week','upc')

    df_modified_groups = df_modified_groups[output_features]

    # rename variable: variable -> upc + variable
    df.columns = ["_".join(col).strip().replace('-','_') 
            for col in df.columns.values]

    return df_modified_groups

def fourier():
    """Seasonality feature from "sales" data.  Subtract Holt-Winters trend, and
    apply fourier analysis.
    """
    df_store_data = pickle.load(open('tmp_df_store_data.out','rb'))
    key = ('dollars','00-01-09125-00005')
    df_store_data = df_store_data[key].reset_index()

    fft_sd = np.fft.fft(df_store_data)
    df_amp_sd = pd.Series(np.absolute(fft_sd))
    # df_amp_sd.plot()
    # plt.show()

def formula_string(response,features):
    """Create string for statsmodel input."""
    return str(response) + " ~ " + " + ".join(features)

def model(df,response,features):
    """Perform multiple regression on data from dataframe with response variable
    name and feature names specified.  
    """
    # set up input string
    fstring = formula_string(response,features)    

    # regression
    est = smf.ols(fstring,df).fit()
    print(est.summary())

def main():
    # dataframe definitions
    # df_store_demo = pd.read_sql_query('select * from store_demographics',conn)
    # df_user_demo = pd.read_sql_query('select * from user_demographics',conn)
    # df_store_data = df_store_data[sd_features]
    # df_user_purchase = pd.read_sql_query('select * from user_purchase_details',conn)
    # df_prod_att = pd.read_sql_query('select * from product_attributes',conn)
    # get dataframe
    df = pickle.load(open('tmp_df_store_data.out','rb'))

    # get response and features labels
    var = pickle.load(open('tmp_dfpca.out','rb')).iloc[:52].apply(
        lambda x: np.real(x))['variates']
    response = var.iloc[0]
    features = var.iloc[1:].tolist()

    # model(df,response,features)
    # pca on dataframe
    # pca(df)

    # sql = 'select * from store_data where year = 2012;'
    # df_store_data = pd.read_sql_query(sql,conn)

    # create regression data columns, i.e., multi-index the data: 
    # (features, upc) & order by week
    # df_store_data = features_setup(df_store_data)
    # df_store_data = df_store_data.fillna(0.0)

    # pca(df_store_data)


if __name__ == '__main__':
    main()

