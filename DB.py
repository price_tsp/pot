####################################################################################################################################################################################
# First thing make sure you pip install psycopg2, and boto 
# You will need to first create a bucket in S3 for your own. This bucket will be your transit point between redshift and your query. 
# The bucket will basically hold a zipped csv query. It is important to create just an empty bucket for this task and leave it do not save any files 
# Every time you run these two functions the bucket is deleted 
# The way the code works to query huge amounts of data is by doing the following two steps first unload your query to redshit then download the file from s3 to your machine
####################################################################################################################################################################################
# This function takes in an sql query, along with the redshift credentials, and S3 bucket path and keys. 
# Your query is downloaded to Amazons storage place called S3 in seconds.  It is baisically an efficient and a fast way to query large sizes of data 
def UnloadRedShiftToS3(query, HOST, PORT, DBNAME, USER, PASSWORD, BUCKETPATH,S3KEY,S3PASS):
   import psycopg2 
   conn_url = "host=" + HOST + " port=" + PORT + " dbname=" + DBNAME + " user=" + USER + " password=" + PASSWORD + " sslmode=require"
   conn = psycopg2.connect(conn_url)
   cur =  conn.cursor()
   query2= "UNLOAD ('"+query+"') TO 's3://"+BUCKETPATH+"' CREDENTIALS 'aws_access_key_id=" + S3KEY +";aws_secret_access_key="+S3PASS+"' DELIMITER AS '\t' PARALLEL OFF gzip;"
   cur.execute(query2)
   conn.commit()
   cur.close()
   conn.close()

# After Unloading into S3 this function basically takes all the contents of the bucket and download a zipped csv to your machine using your internet speed. 
# Baisically you can run this after running the first query. 
def DownloadFilesFromS3(bucket_name,AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY,DESTINATION_PATH):
   import boto
   import sys, os
   from boto.s3.key import Key
   conn = boto.connect_s3(AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY)
   bucket = conn.get_bucket(bucket_name)
   bucket_list = bucket.list()
   for l in bucket_list:
       keyString = str(l.key)
       if not os.path.exists(DESTINATION_PATH+keyString):
           l.get_contents_to_filename(DESTINATION_PATH+keyString)