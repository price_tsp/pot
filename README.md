# README 

The requirements and the approach are communicated in this md file. Please read the following and build your code accordingly and pay attention to the following and read carefully: 

 - If you need to communicate with any of the project owners if you have any questions you can reach us immeditaly on slack channels.
 - We prefer if you assign yourself tasks on our project managenet tool called Taiga which is very similar to Jira. This way we can know about your plan and progress, and so we can advise you on priorities or approaches to make your life easier. 
 - We will have a sprint meeting for 30 to 60 minutes every 2 weeks so that you can reflect on what you have done, show us any demos if any, and discuss approaches or milestones. Ofcourse we will be available whenever you need us just ping us on slack and we can reach out to you as soon as possible.  
 - You are free to make your own work schedule but by the end of each work session, you are required to push your code everytime you have done some coding, that way we can have a backup and we can monitor your activity and do code reviews. 
 - Your repository is totally manged by you feel free to work whichever way you please, if you want to create your own branches thats ok or if you want to keep pushing in the master branch thats ok as well. However, you are requied to write us a readme.md file to describe how to run your code when it is ready to run, and describe the colomns of the output tables in redshift. 
 - To access our posgress SQL-redshift for the data please use the following credintials: 
 	Host: "retail-analytics.cmrxcu1tepas.us-west-2.redshift.amazonaws.com", port: "5439", DB Name: "iridataset", username: "oliver" password: "qnTT5+9*45eeW!jodP"	
 - To access S3 use the following keys, Access Key: "AKIAI42NJJWDBJXSWOEA", Key Pass: "s5i9cBGNixjjOl0mY5ijmQlwcjylW3lLK+PRMQMs"
 - You encourage you to use S3 to unload your exhaustive huge query into redshift if the size of the data is large, please refer to the DB.py file for an example and read the comments. 
 - Please note that your peers will be using the same access keys and redshift passwords so please do not delete any tables unless you let us know what you exactly want to delete. Unless you want to delete a table you have personally created yourself in case something went wrong then sure you can go ahead. 
 - The delivrables of the project is pretty much a bunch of sql tables as your code writes these tables. Please create your own schema before you create the tables, DO NOT add data or create tables in the public schema.
 - Please create a seperate schema for each usecase and call it for example "customer_segmentation_output"
 - Please add a timestamp colomn to your output tables. 
 

Wish you the best of luck and remember don't think of us as if we are your boss, baisically we are here to help you achieve your best and learn new things from each other :)  

# Price Optimization:

## General Requirements and Objective:

Over the past few years there has been renewed focus on the use of price as a merchandising lever. This is due to a number of factors including volatility in commodity prices and the strained consumer purse as a result of wider economic issues. Considering the need to balance the consumer's desire for low price need to maximize value and the adoption of sophisticated solutions has not been as high as some analysts had anticipated. 

## What the Stake-holder needs ?

What is the best price that can achieve the highest ROI ?
How can these numbers be justified ? 

## Dataset to Use:

- weather data: https://pypi.python.org/pypi/get-weather-data 
- economic data: https://www.quandl.com/data/FRED-Federal-Reserve-Economic-Data?keyword= 
- External Events: http://api.eventful.com/docs/events/search
- store_data
- store_demographics
- product_attributes

## Approach to follow 

### 1) The prediction Model part:
 
Basically use the same demand random forest regression model using the same features formulated to predict for the number of units sold week by week. The idea is that we will iterate on the prediction model with different prices and select the one that maximizes the profit. The model should be able to accommodate different business needs in terms of training, as it should be the following. These should be specified from the configuration file or constants file or something. 
 
a) `store dependent`: one model per store for all items

b) `item dependent`: one model per item for all stores

c) `store and item dependent`: one model per item per store

d) `category and store dependent`: one model per store and category

The model will basically predict the demand based on price and promotion features if any. There will be two types of optimization that should be applied This will be explained in details in the next section. 

### 2 Price Optimization from Prediction model: 

Basically we will use want to maximize the profit returns based on the following equation `r=RF(Q) * p`, where `r` is the returned profit, `RF()` is the machine learning model used to predict the demand subject to `Q` which is a set different features and variables that will defined next. Finally `p` is the bounded price that varies between a minimum price margin, referred to as `p_l`, and a maximum price which is referred to as `p_h`. 

- `p_l` is basically bellow the minimum price history of the [item, vendor] through out the historical values by 10%

- `p_H` is basically bellow the maximum price history of the [item, vendor] through out the historical values by 10%

There are basically two types of optimizations that should be done: 

a) Optimization based on only the price: 

The objective function: `argmax_p  RF(Q) * p`, whereas `Q=[p]` and `p_l <= p <= p_h`

b) Optimization based on promotions and the price:  

The objective function: `argmax_p  RF(Q) * p`, whereas `Q=[p, f, d]` and `p_l <= p <= p_h` where `f`, and `d` are the features of the promotions and display type. 

### 3) The Input from the UI or dashboard:  

As this is part of an interactive dashboard the following will be the input from the dashboard:

- item or product to know recommended price, [vendor, item]
- whether they plan to put it on promotions or not [f, d] 

### 4) The output required from the algorithm: 

- predicted demand (based on only the price optimization only)
- best price (based on only the price optimization only)
- expected return based on the best price (based on only the price optimization only)
- predicted demand (based on the price and promotion features optimization)
- best price (based on the price and promotion features optimization)
- expected return based on the best price (based on the price and promotion features optimization)

## Useful links: 

- [ibm] https://www.ibm.com/developerworks/community/blogs/jfp/entry/price_optimization?lang=en
- [elasticity r] http://www.salemmarafi.com/code/price-elasticity-with-r/ 
- [elasticity python] https://github.com/andrewwowens/PED_Statsmodels
- https://www.analyticsvidhya.com/blog/2016/07/solving-case-study-optimize-products-price-online-vendor-level-hard/ 
- [ideal_price1] http://www.dummies.com/education/economics/how-to-determine-the-ideal-price-with-price-elasticity-of-demand/ 
- [ideal_price2] http://rstudio-pubs-static.s3.amazonaws.com/14661_54be2bb6453f4ceb9452c284b6c6e11e.html 
- [events_api] https://api.eventful.com/libs/python/ 
- [events_serarch] http://api.eventful.com/docs/events/search 
- [events_category] http://api.eventful.com/docs/categories/list  
- https://rpubs.com/KakasA09/FARCon082015 


# Traffic Route Optimization:

## Requirements and Objective:

Transportation in Supply Chain and Demand is a major concern and sometimes it can be an exhaustive process and it can be costly. Efficient means of transportation can drastically reduce cost and resources. In this use case we will basically build a code that takes a set of geocode location data (longitude and latitude) which are basically the destinations required to be traveled from a start geolocation point. The solution should account for traffic and shortest distance.  

## Dataset to Use:

google API maps: https://developers.google.com/maps/documentation/javascript/distancematrix 

## How to do it ?  

This can be solved by using the famous Held-Karp algorithm, the way this algorithm works is firstly it calculates the distances between all points of all possible combinations. Then it finds the most optimized route by minimizing the sum of distances traveled for all points. Please follow the example in the file "TSP.py", as you can find it in your repo. In the example you will see a function called `find_dist()` in this function it basically takes 2 geocodes and it calculates the distance between them based on Haversine formula. We won't need that and we need to use google API to get this distance which will be based on traffic information and most optimized route. It will also be nice if you can find a way to retain the routing information from the API. The distance matrix API will basically return to you a distance and an estimated travel time between two geocode points. We need to use the returned estimated time to minimize using the Held-Karp algorithm. The algorithm is already implemented for you in the .py file as you can see, you just need to basically write a new function to be use google API instead of using the Haversine method. The other thing you are required to do is to build an R Shiny App that basically takes in a bunch of latitude and longitude data and uses your python code to find the most optimized route then displays it on the map.

# Useful links:

- https://en.wikipedia.org/wiki/Held%E2%80%93Karp_algorithm
- https://developers.google.com/maps/documentation/javascript/distancematrix