#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

Weekly weather statistics at store location areas.

"""
import datetime
import time
from noaaweb import load_save_csvfile
import pandas as pd

def parse_output(f_out='out.dat'):
    with open(f_out,'r') as f:
        header = f.readline()
        header = header.split('","')
        lines = list()
        for line in f.readlines():
            lines.append(line.split('","')[:-1])
    return header,lines

def days_of_week(year,week):
    """ Input: specifed year and week
        Output: list of dates in that week
    """
    # get time stamp for first day of week in the year
    atime = time.strptime('{} {} 1'.format(year, week), '%Y %W %w')
    
    # convert to datetime object
    date = datetime.datetime.fromtimestamp(time.mktime(atime)).date()
    
    # get all dates of the week
    daysofweek = [date]
    for dayinweek in range(1,7):
        daysofweek.append(date + datetime.timedelta(days=dayinweek))
    return daysofweek

def main(f_in = 'in.dat',f_out = 'out.dat'):
    """Get weather data statistics for each week of specified year.
    """
    # Set up input data and create input file
    zipcode = 70503
    year = 1999
    week = 50
    dates = days_of_week(year,week)
    with open(f_in,'w') as fin:
        no = "2000"
        all_rows = list()
        for i,day in enumerate(dates):
            row_to_file = map(str,[no,i,zipcode,day.year,day.month,day.day])
            all_rows.append(",".join(row_to_file))
        # write file header and rows
        fin.write('no,uniqid,zip,year,month,day'+'\n')
        for row in all_rows:
            fin.write(row+'\n')

    # Make call for weather data, writes out to file
    load_save_csvfile(f_in,f_out)

    # Parse output file
    header, lines = parse_output()

    # fields desired 
    labels = ['Lat','Lon','DAPR','PRCP','TMAX','TMIN']
    indices = [8,9,15,21,25,26]
    indlab = zip(indices,labels)

    df = pd.DataFrame({"Indicies":indices,"Labels":labels})
    df = df.set_index("Indicies")

    for i,line in enumerate(lines):
        column_name = "Day "+str(i)
        df[column_name] =  pd.Series(line)[df.index]

    # convert to float and divide temps by 10. to get Centigrade
    df = df.convert_objects(convert_numeric=True)
    keys = [key for key in df]
    keys.pop(0)
    temps = [25,26]
    for temp in temps:
        for key in keys:
            value = df[key].loc[temp]
            df[key].loc[temp] = value/10.

    # Take the means of the dataframe
    max_temp = df.max(axis=1).loc[25]
    min_temp = df.min(axis=1).loc[26]

    stats = {"max_temp":max_temp,"min_temp":min_temp}
    print(stats)

if __name__ == '__main__':
    main()